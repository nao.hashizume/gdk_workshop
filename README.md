## GDK Workshop

In this workshop, participants will:

* Learn about the tools used within GDK.
* Add a new diagnostic check to the `gdk doctor` command.
* Write tests for this diagnostic check.

## Prerequisites

You will require:

* A computer with either macOS or Linux operating system.
* GDK installed and fully working on your system, ready for use during the workshop.

## Setup

### Install a new version of Redis

First, install any new version of Redis that you prefer. For example:

* For macOS: `brew install redis`.
* For Ubuntu/Debian: `sudo apt-get install redis`.

### Temporarily update PATH variable

For this workshop, we need to use the Redis version installed via Homebrew. To do this, we will temporarily update the `PATH` variable in the current terminal session:

Add Homebrew's bin directory to the start of your `PATH`:

```shell
PATH="/opt/homebrew/bin:$PATH"
```

This ensures that the terminal searches Homebrew's directory first when we run a Redis command.

### Restart Redis service

Restart the Redis service to apply changes:

```shell
gdk restart redis
```

### Verify the Redis server version

Check the current Redis version:

```shell
redis-server --version
```

Notice that the output shows the new version, rather than the version defined in `.tool-versions`.

### Challenge

You're now ready to add a new diagnostic check to the `gdk doctor` command! :checkered_flag:

Ensure it can detect when the Redis version being used does not match the version specified in the `.tool-versions` file, so users can resolve this discrepancy. For example:

```shell
Redis
================================================================================
Redis version 7.2.4 does not match the expected version 6.2.13 in .tool-versions.
Please check your `PATH` for Redis with `which redis-server`. You can update your PATH to point to the correct version if necessary.
```

### GDK doctor conventions

For today's challenge, we'll focus on:

* `lib/gdk/diagnostic.rb`: This file is where all diagnostic checks are listed and loaded.
* `lib/gdk/diagnostic`: This directory is where you'll add the Redis version check file. For a good example, you can check `lib/gdk/diagnostic/postgresql.rb`. :bulb:

### Use a Shellout

Shellout is often used in gdk to execute shell commands. To find the available methods, check `lib/shellout.rb`.

### Add a simple test

Ensure to mock Shellout usage in your tests.

### Extra Challenges :moneybag:

For those looking for extra challenges to contribute to GDK, here's a list of issues you can tackle to expand the `gdk doctor` command:

* Check for a valid license configuration ([#1820](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/1820))
* Detect when gitlab gems are missing ([#907](https://gitlab.com/gitlab-org/gitlab-development-kit/-/issues/907))
